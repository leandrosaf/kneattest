
var calculate = {
	//Calculate resupply per ship and print
	stopsNeeded: function stopsNeeded(distance, consumables, MGLT) {
		if (distance === undefined || consumables === undefined || MGLT === undefined)
			return null;

		if (MGLT == 'unknown')
			return 'unknown';
		
		return Math.trunc(distance / (this.consumablesToHours(consumables) * parseInt(MGLT)));
	},
	consumablesToHours: function toHours(period) {
		var base = 0;
		if (period.indexOf(' year') > -1)
			base = 365 * 24;
		if (period.indexOf(' month') > -1)
			base =  30 * 24;
		if (period.indexOf(' week') > -1)
			base = 7 * 24;
		if (period.indexOf(' day') > -1)
			base = 24;
		if (period.indexOf(' hour') > -1)
			base = 1;

		if (base > 0) {
			return base * parseInt(period.substr(0, period.indexOf(' ')));
		}
		return 'invalid';
	}
}

module.exports = calculate;