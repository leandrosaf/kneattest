Synopsis
This system uses a web API available at swapi.co/api/ to get data from starships and calculate their efficiency.

You input the distance that the ships should travel and system will calculate how many stops to resupply they need.


Installation
To run the program you will need to have Node.js. If you don't, follow the instructions here https://nodejs.org/ first.

After installing Node.js, you need to install the packages. Open the command window of your operating system and navigate to the folder where you extracted the files. Execute the command:
npm install

That will install the dependencies for this project. After the installation process finishes, you can execute the following commands to run the program:
npm start or node index.js


Tests
The tests were implemented with mocha and chai. You can visit their website for more information on https://mochajs.org/ and http://chaijs.com/.

Basically you have to install the dependencies on your project:
npm install mocha --save-dev
npm install chai --save-dev

And then, execute the following command to see the test report:
npm test or mocha tests --recursive

You can eventually use a --watch parameter with mocha to run the tests automatically after changing files.
