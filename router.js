/**
 * Please note: This is a straight forward api consumption
 * A prodution ready system should have a more complex data layer
 */
var https = require('https');

module.exports.getStarships = function (query, success) {
	var options = {
		host: 'swapi.co',
		port: 443,
		path: '/api/starships/' + query,
		method: 'GET'
	};
	var answer = '';
	// GET request
	https.request(options, function (res) {
		//console.log("statusCode: ", res.statusCode);
		res.on('data', function (data) {
			answer += data;
		}).on('end', function () {
			return success(answer);
		}).on('error', function (e) {
			console.error(e);
		});;
	}).end();

	//reqGet.end();
	// reqGet.on('error', function (e) {
	// 	console.error(e);
	// });
};


// //This is a straight foward approach for only getting starships
// //A complete solution should have a http layer for generic access
// var https = require('https');

// var API = function () {
// 	var options = {
// 		host: 'https://swapi.co/api',
// 		port: 443,
// 		path: '/starships/',
// 		method: 'GET'
// 	};

// 	https.request(options, function (res) {
// 		res.setEncoding('utf8');
// 		res.on('data', function (result) {
// 			if (res.statusCode.toString().startsWith('2')) {
// 				return JSON.parse(result);
// 			}
// 		});
// 	}).end();
// };
