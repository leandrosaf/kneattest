//This is just an examplo of Node test using mocha and chai
//To cover better the app I should be more modular

var chai = require('chai');
var assert = chai.assert;
var calculate = require('./helper.js');

describe('RouterAPI', function () {
	//Test if the function stopsNeeded will break
	it('stopsNeeded() should return null if it misses any parameter', function () {
		assert.equal(calculate.stopsNeeded(), null);
	});
	it('stopsNeeded() should return "unknown" if the ship MGLT is unknown', function () {
		assert.equal(calculate.stopsNeeded(1000000, '3 days', 'unknown'), 'unknown');
	});

	//Test if the function consumablesToHours will break
	it('consumablesToHours should return 3 days in integer hours', function () {
		assert.equal(calculate.consumablesToHours('3 days'), 72);
	});
	it('consumablesToHours should return 1 year in integer hours', function () {
		assert.equal(calculate.consumablesToHours('1 year'), 8760);
	});
	it('consumablesToHours should return 4 weeks in integer hours', function () {
		assert.equal(calculate.consumablesToHours('4 weeks'), 672);
	});
	it('consumablesToHours should return 1 month in integer hours', function () {
		assert.equal(calculate.consumablesToHours('1 month'), 720);
	});
	it('consumablesToHours should return "invalid" from unknown periods', function () {
		assert.equal(calculate.consumablesToHours('5 minutes'), 'invalid');
	});
});