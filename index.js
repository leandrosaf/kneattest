//This is a simple application to consume API
//Its architecture is far from ideal to a production systems
var router = require('./router.js');
const readline = require('readline');
var calculate = require('./helper.js');

var shipList = [];
var page = '?page=1';

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.question('To calculate how many stops for resupply inform the distance to cover: ', (distance) => {
	console.log('Downloading ship data...');
	//Calls the first page of ships
	router.getStarships(page, returnList);

	function returnList(data) {
		var answer = JSON.parse(data);

		shipList = shipList.concat(answer.results);

		//Keep calling the next pages
		if (answer.next) {
			page = answer.next.substr(answer.next.indexOf('?'), answer.next.length - answer.next.indexOf('?'))
			router.getStarships(page, returnList);
		}
		else {
			//Only calculate resupply when there is no more pages to load
			printStopsPerShip(shipList);
		}
	};

	//Calculate resupply per ship and print
	function printStopsPerShip(shipList) {
		console.log('Stops required:');
		shipList.forEach((ship) => {
			console.log(ship.name + ': ' + calculate.stopsNeeded(distance, ship.consumables, ship.MGLT));
		});
	}

	rl.close();
});
